// console.log("Hello World!");



// REVIEW from yesterday's lesson:


for (let number = 0; number < 10; number++) {
	if (number == 2) {
		console.log("Number 2 is found");
		continue;
	}

	if (number != 2) {
		console.log(number);
	}

	if (number == 5) {
		break;
	}
}



/*

INSTRUCTION:

1. initialize a value
2. check the condition
3. runs the statements inside the loop if the condition is true
4. change of value (increment or decrement)
	- go back to step 2

*/




// ----------------------------------------------





// RE-ASSIGNING WITH CONCATENATION OF STRING


// we want to display and connect all the letter O from the word dooooom 


let myString = "dooooom";
let letterO = "";

for (let i = 0; i < myString.length; i++) {
	if (myString[i] == 'o') {
		letterO = letterO + myString[i];
	}
}

console.log(letterO);


/* 

PROCESS:

"" = empty string

it will not run d because its not = to o

empty string + o = o
       o     + o = oo
       oo    + o = ooo
       ooo   + o = oooo
       oooo  + o = ooooo

it will not run m because its not = to o

loop stops here

*/









// ----------------------------------------------





// ARRAY


// an array in programming is simply a list of data. Let's write the example earlier





// ----------------------------------------------


// WAYS TO WRITE AN ARRAY: 


// A
let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';



// B
let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];


// index # 0 = 2020-1923
// index # 2 = 2020-1925




// ----------------------------------------------


// COMMON EXAMPLES OF ARRAYS:


let grades = [98.5, 94.3, 89.2, 90.1, 99];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];



// index # 0 = 98.5
// index # 3 = 90.1

// index # 0 = Acer
// index # 3 = Neo


console.log(grades);
console.log(computerBrands);





// ----------------------------------------------

// OTHER WAY to write array but NOT RECOMMENDED for USE:

let mixedArray = [12, 'Asus', null, undefined, {}];
console.log(mixedArray);





// ----------------------------------------------


// ALTERNATIVE WAYS TO WRITE AN ARRAY:


let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake bootstrap'
	];

console.log(myTasks);








// ----------------------------------------------


// CREATING AN ARRAY WITH VALUES FROM VARIABLES


let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);







// ----------------------------------------------


// .LENGTH PROPERTY



// The .length property allows us to get and set the total number of items in an array

// length property can also be used with strings. SOme array methods and properties can also be used with strings




console.log(myTasks.length);
console.log(cities.length);




// ANOTHER EXAMPLE:


let fullName = "Jamie Oliver";
console.log(fullName.length);






// ANOTHER EXAMPLE WITH a BLANK ARRAY:



let blankArr = [];
console.log(blankArr.length);







// ANOTHER EXAMPLE REMOVING THE LAST ELEMENT IN AN ARRAY:



/*

SCROLL UP

let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake bootstrap'
	];

console.log(myTasks);

*/



myTasks.length = myTasks.length-1
console.log(myTasks.length);
console.log(myTasks);





// Another Example in removing the last part



/*

SCROLL UP

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

*/



cities.length = cities.length-1
console.log(cities.length);
console.log(cities);






// ----------------------------------------------



// .length can't be applied on strings


console.log("Initial length of fullname: " + fullName.length);

fullName.length = fullName.length-1;
console.log("Current length of fullname.length: " + fullName.length);




/*

this will not work!!

let theBeatles = ['JOhn', 'Paul', 'Ringo', 'George'];
the theBeatles++;
console.log(theBeatles);

*/






// ----------------------------------------------



// DISPLAY THE SPECIFIC ELEMENT OF AN ARRAY THROUGH INDEX


// let grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades[0]);



// let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

console.log(computerBrands[3]);






//Long Wat to Access the last element

// let grades = [98.5, 94.3, 89.2, 90.1, 99];
function getGrade(index) {
	console.log(grades[index])
}

getGrade(3);




// Short cut to Access the last element


// let grades = [98.5, 94.3, 89.2, 90.1, 99];
getGrade(grades.length-1);



// added 99 element
getGrade(grades.length-1);





// OTHER EXAMPLE


let lakersLegend = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
let currentLaker = lakersLegend[2];
console.log(currentLaker);





// ----------------------------------------------



// CHANGE AN ELEMENT / RE-ASSIGNING ARRAY VALUES


console.log('Array before reassignment');
console.log(lakersLegend);

lakersLegend[3] = "Paul Gasol";

console.log("Array after reassignment");
console.log(lakersLegend);








// CHANGE THE LAST ELEMENT



let bullsLegend = ['Jordan', 'Pippen', 'Rodman', 'Kukoc'];
let lastElementIndex = bullsLegend.length-1;
console.log(bullsLegend[lastElementIndex]);


// OR like this:


/*

let bullsLegend = ['Jordan', 'Pippen', 'Rodman', 'Kukoc'];
console.log(bullsLegend[bullsLegend.length-1]);

*/





//RE-ASSIGN

bullsLegend[lastElementIndex] = "Harper";
console.log(bullsLegend);




// add item into the array

// Adding elements after the last element



let newArr = []
console.log(newArr[0]);


// no value for 0 because their is no value at all, it's blank





// ADD CONTENT on the blank array


newArr[0] = "Cloud Strife";
console.log(newArr);

// newArr[1] is blank

newArr[2] = "Tifa Lockhart";
console.log(newArr);



// ACCESS / DISPLAY THE LAST LENGTH

newArr[newArr.length] = "Barret Wallace";
console.log(newArr);










// ----------------------------------------------



// LOOPING OVER AN ARRAY (to display per element of an array)




for (let index = 0; index < newArr.length; index++) {
	console.log(newArr[index]);
}




// Another EXAMPLE USING MODULO % (always === to 0)


// a loop that will check per element if divisible by 5


let numArr = [5, 12, 30, 46, 40];

for (let index = 0; index < numArr.length; index++) {
	if (numArr[index] % 5 === 0) {
		console.log(numArr[index] + " is divisible by 5");
	}

	else {
		console.log(numArr[index] + " is not divisible by 5");
	}
}








// ----------------------------------------------



// MULTI-DIMENSIONAL ARRAYS (ARRAY ON AN ARRAY)

// arrays inside an array



let chessBoard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'], 
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
	['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
	['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
	['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
	];

console.log(chessBoard);

console.log(chessBoard[1][4]);    // [row][column]

console.log(chessBoard[4][2]);    // display c5

console.log("Pawn moves to: " + chessBoard[1][5]);





